+++
title = "About"
description = "About dradux"
date = "2019-02-28"
aliases = ["about-us", "about-dradux", "contact"]
author = "drad"
showShare = false
+++

dradux was created on June 2nd, 2013 as a place to share the draduxOS code, resources, and related information. Over the years dradux has transformed into more than the draduxOS; the blog and other resources have gained traction as a place to share services and information and continue to grow.

dradux will continue to evolve over the years but it is expected to keep the draduxOS and blog content as key to the site and its services.
