+++
title = "Contact"
description = "Contact us"
date = "2022-05-26"
aliases = ["contact",]
author = "drad"
showShare = false
+++

<link rel=stylesheet href=/css/contact.css>
<script src="/js/jquery-3.6.0.min.js"></script>
<script src="/js/jqBootstrapValidation.min.js"></script>
<script src="/js/contact.js"></script>


If you have a question/comment, or simply want to say hi please contact us!


<div class="contact-form-wrapper">
    <p class="contact-header">
        Contact Us
    </p>
    <form id="contact-form" class="contact-form" method="post" action="#">
      <div class="form-group row">
        <label for="name" class="col-4 col-form-label">Name</label>
        <div class="col-8">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-user"></i>
            </div>
            <input id="name" name="name" placeholder="Please enter your name" type="text" required="required" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="email" class="col-4 col-form-label">E-mail</label>
        <div class="col-8">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </div>
            <input id="email" name="email" placeholder="Your e-mail address" type="text" required="required" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="subject" class="col-4 col-form-label">Subject</label>
        <div class="col-8">
          <div class="input-group">
            <div class="input-group-addon">
              <i class="fa fa-envelope"></i>
            </div>
            <input id="subject" name="subject" placeholder="What is this about?" type="text" required="required" class="form-control">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label for="message" class="col-4 col-form-label">Message</label>
        <div class="col-8">
          <textarea id="message" name="message" cols="40" rows="10" required="required" class="form-control"></textarea>
        </div>
      </div>
      <div class="form-group row">
        <div class="offset-4 col-8 action-buttons">
          <button name="submit" type="submit" class="btn btn-primary">Send</button>
        </div>
      </div>
    </form>
    <div class="contact-footer">
        Please read our <a href="/privacy-policy">Privacy Policy</a> (we do not share, sell or otherwise provide your infomation without your consent unless required by law)
    </div>
</div>
