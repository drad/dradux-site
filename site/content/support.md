+++
title = "Support"
description = "Support"
date = "2022-05-26"
aliases = ["support", "support-us"]
author = "drad"
showShare = false
+++

If you need support with any of our services/information please <a href="/contact">contact us</a>.

If you would like to support us we accept bitcoin or <a href="/contact">contact us</a> to discuss other methods.

### Donations Address

bc1qxl8fhu7vt8shpavkyvqvcvtmh20elv64aafz59
![QR Code for bc1qxl8fhu7vt8shpavkyvqvcvtmh20elv64aafz59](/images/qr-bc1qxl8fhu7vt8shpavkyvqvcvtmh20elv64aafz59.png)
