+++
title = "draduxOS"
description = "a lean, minimal linux distribution"
date = "2013-06-02"
aliases = ["draduxOS", "linux", "debian", "arch", "artix"]
author = "drad"
toc = true
+++

From its inception, the goal of draduxOS was to be lean, lightweight, stable (in terms of user experience), and maintainable. draduxOS has evolved through the years but the user experience has remained consistent:
> a minimal UI based on OpenBox with dmenu launcher and keyboard shortcuts performing the heavy lifting

Our core philosophy is that the OS and desktop environment/desktop manager should provide their features with as little impact on the user as possible while giving maximum flexibility:
> the user should be able to customize the experience to suite their need with as little difficulty as possible

## Core Principles

* __secure:__ you know what is installed because you install it (yes you are trusting a core OS distributor but this is as close to "building it all yourself" as you can get without building it all yourself)
* __lean:__ your computer will be as lean as possible as you only install what you want/need
* __minimal:__ in terms of resource usage (CPU/memory) and wasted screen real-estate
* __keyboard driven:__ we strive to have everything keyboard driven as this makes for a more fluid user experience in our opinion
* __consistent:__ less change for the sake of change

## Features

* __rolling:__ draduxOS is a rolling distro, this means you "upgrade" as a normal part of the software upgrade process - you are always running the latest and do not need a full rebuild or monolithic upgrade release process
* __no systemd:__ systemd is not simple and modular but rather the opposite - I've always found [OpenRC](https://wiki.gentoo.org/wiki/OpenRC) to be more than adequate at a fraction of the cpu/memory cost

## What's Not Included

* ZFS is installed by default but no pool is created as this process is highly dependent on your hardware, needs, and usage. A few tips if you do use ZFS:
    - encryption incurs a ~64% performance hit (both read and write) - e.g. a read of ~30k down to ~10k / write of ~10k down to ~3.5k
    - if you are using a HDD and have a SDD also, put your ZIL and L2ARC on the SSD to give you near SSD performance on your HDD
    - create your filesystems in a manner that leverages your snapshot strategy

## Installing

Installing draduxOS may seem more complicated (or take more time) than other distros but it can easily be done in less than an hour!

Here are the steps to getting up and running with draduxOS:

* __[Prepare](#prepare):__ download the base system, burn it to a thumb drive, and lay out your disk partition
* __[Install Base](#install-base):__: install base system
* __[Provision](#provision):__: adds the draduxOS packages and performs the base setup of the OS


### Prepare

Download [Artix](https://artixlinux.org/download.php) base of your choice - we recommend the Artix-base-openrc version and this guide is focused on OpenRC (if you go with another init system you will have to adjust accordingly).

{{% notice warning "Verify Your Download!" %}}
Verify the download with the PGP key provided to ensure the ISO you get is official (see instructions on download page)!
{{% /notice %}}

Burn the ISO to a thumb drive using `cat` or `dd`:

* cat is faster: `cat /path/to/artixlinux.iso > /dev/sdX && sync`
* or use dd: `dd if=/path/to/artixlinux.iso of=/dev/sdX bs=8M`
{{% notice note "Adjust Above Device Paths" %}}
You will need to replace `/dev/sdX` with the actual device you wish to write to.

__You must write to the device__ (not a partition on the device) as the ISO needs to create a MBR (and likely other partitions).
{{% /notice %}}

Determine your disk layout. You should have a `boot`, `swap` and `root` partition at a minimum. It is a good idea to have a `home` partition and possibly a `var` partition. Its often helpful to create a spreadsheet with your disk(s) space and the partitions you need to help calculate sizes for each partition.
{{% notice note "Recommendations" %}}
- use [zfs on linux](https://zfsonlinux.org/) where possible/feasible due to the flexibility it brings (plus snapshots are awesome)
- use GTP partitioned disks due to their support of larger disks
{{% /notice %}}
{{% notice info "ZFS Encryption Performance" %}}
Encrypting a dataset incurs a significant disk read/write hit (as with any filesystem). Currently (2022-06-17) ZFS does not support some newer encryption algorithms (notably Curve25519) as CCM and GCM are the only available algorithms. GCM is notably faster than CCM, my findings for GCM are as follow:
- read: 64.5% slower (29911.19 vs 10611.57)
- write: 64.5% slower (9952.55 vs 3530.86)
{{% /notice %}}

#### Example Partition Layout

| Partition | Format | Size | Notes |
| --------- | ------ | ---- | ----- |
| boot | ext2 | 500M | no need for journaling or anything fancy|
| swap | swap | 2G | minimum of 2gb, a good rough estimate is to make this the same (or larger) as the amount of RAM you have as you will need it that large to hibernate if you use that|
| root | xfs | * | the size depends on many other things, if `/var` and `/home` are on their own partitions 12G-14G should be enough|

If you are using ZFS, don't forget to create a partition for your pool. Also note that if you happen to have two disk (one SSD and one HDD) it will likely increase read/write speeds significantly if you create partitions for a ZIL and L2ARC on your SSD. This will give you close to SSD disk speed on your HDD!


### Install Base

You can now plug the USB drive in and boot from it. Install the Artix base using the [Installation Guide](https://wiki.artixlinux.org/Main/Installation).

{{% notice info "Install time" %}}
The process is straightforward and should take ~20-30 minutes. You can stop after the "Reboot the system" step as the "Provision" section below will handle the remaining items.
{{% /notice %}}

Install the following packages during the "Localization" step where nano is installed. To do so change:

```
 pacman -S nano
 nano /etc/locale.gen
```
To be:
```
 pacman -S nano xfsprogs python openssh openssh-openrc
 nano /etc/locale.gen
```
{{% notice warning "Optional xfsprogs" %}}
xfsprogs is __only__ needed if you use an xfs partition above. You can skip installing it but will receive several warning/errors during setup (which can be ignored).
{{% /notice %}}



### Provision

Provisioning of draduxOS is handled by [ansible](https://docs.ansible.com/).
{{% notice note "Notice" %}}
The stock draduxOS ansible setup uses an [OpenRC](https://wiki.gentoo.org/wiki/OpenRC) base, you will need to adjust accordingly if you opt for a different init manager.
{{% /notice %}}
{{% notice tip "Install Ansible" %}}
You can easily install ansible via [pipx](https://pypa.github.io/pipx/) with: `pipx ansible`.
{{% /notice %}}
First,  verify we can reach the computer in question:

```shell
# just ping and expect to get a pong back.
ansible your-server-name-or-ip -m ping --ask-pass
```

We use ssh keys to secure access to servers so we need to deploy our keys:
```shell
# you do not have to deploy ssh keys, you can simply add `--ask-pass` to
# the following command if you did not deploy keys.
ansible-playbook --ask-become-pass --ask-pass --limit your-server-name-or-ip deploy-master-keys.yml

```
Now we can provision draduxOS:
```shell
# before provisioning we recommend you review the draduxOS.yml playbook
# and comment/uncomment any additional packages you may need/want.
ansible-playbook draduxOS.yml --limit your-server-name-or-ip
```

## Inspiration

- Microsoft Windows: definitely provided inspiration on what not to do
- [CrunchBang](https://en.wikipedia.org/wiki/CrunchBang_Linux): the first distro that felt right to me
- [Artix](https://artixlinux.org): the best distro post-systemd infatuation

## Background

I started my *NIX adventure in 1996 on an AT&T UNIX based minicomputer. We used it in a COBOL class I was taking and honestly I had little focus on UNIX. My next step was in 1998 with a HP-UX based server where I was in charge of maintaining customer code. I found UNIX to be easier to work with and much more solid and robust than other OSes at that time. I quickly moved into RedHat 6 (when it was just RedHat - not RHEL) as a free OS to investigate for possible use cases for new customer. I then moved to running RedHat LPars on ZOS mainframes and was hooked on the power and ability.

I spent most of my time on "servers" doing my work from a "client" that was usually Windows. Each iteration of Windows seemed to be slower, have more bloat, and as if it were designed to keep me from doing things on my local machine that I actually needed (or throw road blocks in my way for doing them). I started a new job in 2003 and was on-site with clients 100% of the time and was rarely in my corporate office. This seemed like a great time to move to linux for my work laptop plus the project I was on was again mainly a UNIX (SunOS) project.

I've honestly never looked back since 2003. I have had times where I had to boot a Window VM and definitely had a fair amount of Windows Server support work but my primary work computer has been Linux since 2003. I originally started with Ubuntu as it was the only solid distro for a laptop at that time.

After 2-3 years of Ubuntu (and its growth) I decided it was getting too heavy and I was constantly leaning to needing/wanting lighter so I ventured off to [CrunchBang](https://en.wikipedia.org/wiki/CrunchBang_Linux). Crunchbang was a great distro but honestly I was feeling the pull to build my own Debian before Crunchbang closed shop. I started building my own custom Debian around 2011 and decided to document the process in November of 2012.

## History

draduxOS started as yet another Debian based 'distro' and is more of a guide to building (build as in configure and setup) an OS than a traditional distro.

On 2021-09-10 draduxOS changed from a [Debian](https://debian.org) base to [Arch](https://archlinux.org) (actually [Artix](https://artixlinux.org)) primarily due to systemd. The end-user experience remained the same, system load was drastically reduced, packages were fresher and [AUR](https://aur.archlinux.org/) is awesome!

At some point we may make a full-fledged distro; however this takes time and the guide seems to have worked well over the years.
