+++
title = "Privacy Policy"
description = "Privacy Policy"
date = "2022-05-25"
aliases = ["privacy", "policy"]
author = "drad"
showShare = false
+++

We respect your expectations of privacy when providing information to our us.

### Disclosure of your Personal Information

We do not collect personal information except to the extent you provide that information to use through email, your web browser or through our contact forms. We will not share, sell, or otherwise provide non-public information to others without your consent

However, under the following circumstances, we may be obligated to disclose personal information:

* if required or authorized by law
* if you have consented to disclosure


### Communicating With Us

By submitting messages through our website’s forms, you agree that you have read and agree to the full terms of our website.
