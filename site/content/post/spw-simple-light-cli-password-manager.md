+++
author = "drad"
title = "spw - simple, light, cli password manager"
date = "2020-09-07 05:58:25"
description = "spw is a lightweight cli password manager"
featured = true
tags = ["lightweight", "minimalist", "password", "manager", "cli", "python"]
categories = ["dx-app", "security"]
+++

> spw is a python application that stores and retrieves passwords in a secure maner. spw is designed to be quick, light on resources/dependencies, and command line/script driven.
>
> Passwords are stored in an encrypted format using PKCS1_OAEP encryption. This means you use a public and private key to encrypt and decrypt items stored within the store. This is a secure method of password storage and there is virtually no chance someone (including yourself) can view decrypted passwords without the private key.
>
> spw is intended to provide a secure mechanism to store (and more importantly retrieve) passwords. spw's command-line interface allows easy integration into openbox's keyboard shortcut functionality (or similar tools). spw provides an easy mechanism for copying a password to the clipboard (e.g. C+A+j will copy the gmail junk account's password to your clipboard).
  - [spw - about](https://gitlab.com/drad/spw)

I have used spw daily for several years, the password generator is extremely useful/quick to generate passwords!


You can install spw via pip or pipx: `pipx install spw` or view the [spw gitlab page](https://gitlab.com/drad/spw).
