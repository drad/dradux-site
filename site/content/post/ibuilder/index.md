+++
author = "drad"
title = "ibuilder - the image builder"
date = "2022-05-27"
description = ""
featured = true
tags = ["docker", "python", "typer", "pypi", "rich", "pydantic", "ci", "cd"]
usePageBundles = true
featureImage = 'help.png'
# featureImageAlt: 'Description of image' # Alternative text for featured image.
# featureImageCap: 'This is the featured image.' # Caption (optional).
# thumbnail: 'thumbnail.jpg' # Image in lists of posts.
# shareImage: 'share.jpg' # For SEO and social media snippets.
categories = ["dx-app"]
+++

ibuilder is a [cli](https://en.wikipedia.org/wiki/Command-line_interface) based builder of [docker](https://hub.docker.com/) images. It provides an interface for building and pushing the image as well as for tagging source code after build with a build version and other common image tasks.

Building an image is easy:
![building an image](build.png)

You can also view build history:
![build history](history.png)

### More Info

The [boi Tutorial](/post/the-boi-primer) provides more info on adding ibuilder to an existing application or you may want to check out [ibuilder](https://gitlab.com/drad/ibuilder) for more info.
