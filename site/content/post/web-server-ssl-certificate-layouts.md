+++
author = "drad"
title = "Web Server SSL Certificate Layouts"
date = "2019-08-03 13:59:36"
description = "layout of different web server SSL certificates"
featured = false
tags = ["ssl", "haproxy", "nginx"]
categories = ["devops"]
+++


### HAProxy
All of the following in one .pem file:

1. The Certificate for your domain
1. The intermediates in ascending order to the Root CA
1. A Root CA, if any (usually none)
1. Private Key

### Nginx
Two separate files:

1. The Certificate for your domain, the intermediates, and root CA are in one file
1. The private key
