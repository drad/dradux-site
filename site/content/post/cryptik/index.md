+++
author = "drad"
title = "cryptik - the cryptocurrency ticker"
date = "2022-05-25"
description = "a cli application for displaying the current price of a cryptocurrency"
featured = false
tags = ["pypi", "python", "cli", "crypto", "bitcoin", "ticker", "conky"]
usePageBundles = true
featureImage = 'help.png'
# featureImageAlt: 'Description of image' # Alternative text for featured image.
# featureImageCap: 'This is the featured image.' # Caption (optional).
# thumbnail: 'thumbnail.jpg' # Image in lists of posts.
# shareImage: 'share.jpg' # For SEO and social media snippets.
categories = ["dx-app"]
toc = true
+++

a [cli](https://en.wikipedia.org/wiki/Command-line_interface) application for displaying the current price of a cryptocurrency, designed (but not limited) to be ran in conky. cryptik supports multiple exchanges and multiple currencies.

### Install

Install via pip or pipx: `pipx install cryptik` or view the [cryptik gitlab page](https://gitlab.com/drad/cryptik).

### Setup

cryptik can be ran by passing all options in during the call or you can set up a config file to specify defaults.

### Running

You can run cryptik directly from the terminal to get current prices:

```
$ cryptik
BTMP:BTC $29511.55 @19:57
```

You can also call cryptic from within conky:

```
...
$hr${font}
Mail: ${new_mails}/${mails}${if_existing /tmp/.networkstatup}
${color2}${texeci 600 cryptik}${color}
Shortcuts List: A+F1
${hr}
...
```

Which produces a result similar to the following in conky:

![Example Conky Run](conky.png)
