+++
author = "drad"
title = "Audio Player Comparison"
date = "2014-01-17 21:55:40"
description = "a comparison of lightweight audio players"
featured = false
tags = ["comparisons", "reviews", "music", "audio", "cmus", "vlc", "banshee", "rhythmbox", "amarok", "juk", "exaile", "moc"]
categories = ["reviews"]
series = ["reviews"]
toc = true
+++

### Requirements ###

- must be light on cpu and ram
- small overall footprint
- perform basic audio playing features (playlist management a plus)

### Findings ###

| App | Type | Size | CPU | RAM | Notes |
| --- | --- | --- | --- | --- | --- |
|cmus|ncurses|192,506|3.7%|8.096|really like it, mutt of audio players|
|deadbeef|GUI|2,321,554|5.0%|22,496|basic ui; it loads/plays my playlist!|
|vlc|GUI|1,211,816|10.3%|45,984|A great 'plays anything' player but heavy on runtime resources|
|banshee|GUI|192,506| | |uses mono..., UTTER CRAP! will not play any of my music, GStreamer library error: Shutdown; do not like the interface (discover based)|
|rhythmbox|GUI|446,246| | |does not work Jack error, do not like the interface (discover based)|
|musique|GUI|574,458|9.3%|53,992|wants to scan for music...; very nice looking but doesn't seem to be able to save/load playlists, perhaps need a newer version|
|minirok|GUI|74,524|6.7%|81,052|basic, doesn't seem to have a save/open playlists, does 'reload' last playlist, pulls in a LOT extra files (260+mb)|
|amarok|GUI|5,944,266|21.9%|122,224|pulls in 226mb additional; way to much cruft|
|lxmusic|GUI|93,624| | |basic, seems nice but it will not open any of my music, no errors/notices at all|
|juk|GUI|660,696|10.3%|55,992|adds 163M of additional pkgs; I like the player, nice, simple, asks for search folder, and auto-imported playlist|
|guayadeque|GUI|1,632,512|10.3%|43,636|nice l&f; imports playlists; a bit odd to get around in; has way to many features I'll never use (jamendo, etc.)|
|aqualung|GUI|800,842|5.7%|21,796|mdi interface (bit oldschool looking; loads my playlist!;I like it!|
|bluemindo|GUI|168,876| | |odd interface, difficult to find/do anything, would not play my music|
|exaile|GUI|1,056,136|12.0%|59,220|not bad on install; somewhat intuitave; opens/plays my playlist!|
|moc|ncurses|311,552|4.0%|9,260|I like!; plays my playlist!; simple and straightforward!; very nice, maybe easier to use than cmus|
|yatm|CLI|14,258| | |too basic, just a cli player like aplay; no playlist support|
|yauap|CLI|21,266| | |simple cli player; no playlist support|
|qmmp|GUI|95,514|8.3%|35,156|odd but basic/nice UI; opens/plays my playlist!|

> - cpu % after 1min of play of 5 songs
> - ram (RES) XX after 1min of play of 5 songs

- to find app size (in K): `apt-cache show cmus | grep Size:`

### Test

- install app
- open app
- create playlist with 5 songs
  - If it aint one thing, paramore-misery business, Hit it Again, Foo Fighters - Best of You, Foofighters everlong
- close app
- open app (with timer), open playlist, and play: `appname & top -d 3.0 -n 21 -p $(pgrep -d',' -f appname) && fg`
- open app
- get CPU/Memory stats from top

#### Results
cmus and moc are both good, need to run a larger (more songs and longer run) sample for memory/cpu comparisons.


### Test #2
- playlist has 37 songs (Advent Chamber Orchestra* & Various Artists (Dawn of*)
- play for 45m
- measure results

| App | Type | Size | CPU | RAM | Notes |
|---|---|---|---|---|---|
|cmus|ncurses|192,506|3.0%|8,112| |
|moc|ncurses|311,552|3.0%|10756|I noticed that often cpu utilization for moc was 0% during the run|

#### Notes
- cmus is a bit technical to learn but I do like it
- moc/p is a bit more simple/straightforward
- moc/p has a sort of front and back-end, you can close the ncurses ui and the backend (also mocp) will continue running, the above measurement for moc is with the front-end closed


### Using cmus
- [man page](http://linux.die.net/man/1/cmus)
- [arch wiki](https://wiki.archlinux.org/index.php/Cmus#Configuration)
- :a: ~/music - add files
- v: stop play
- c: pause play
- b: next play
- x: play
- :clear - clear current view
- y - add to playlist
- 1 - library
- 2 - sorted library
- 3 - playlist
- 4 - play queue
- 5 - browser
- 6 - filters
- 7 - settings
- :save -p FILE - saves playlist view to playlist file
- :load -p FILE - load a playlist file to playlist view
- s - toggle shuffle
- r - toggle repeat
- C - toggle continue

### Using moc
- install moc and moc-ffmpeg-plugin
- start with mocp
