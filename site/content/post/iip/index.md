+++
author = "drad"
title = "iip - inspect image and packaging"
date = "2022-05-25"
description = "inspects packages (python, ruby, node, etc) and images (docker) of a project"
featured = true
tags = ["devops", "docker", "pypi", "nodejs", "node", "ruby", "cli", "rust"]
usePageBundles = true
featureImage = 'help.png'
# featureImageAlt: 'Description of image' # Alternative text for featured image.
# featureImageCap: 'This is the featured image.' # Caption (optional).
# thumbnail: 'thumbnail.jpg' # Image in lists of posts.
# shareImage: 'share.jpg' # For SEO and social media snippets.
categories = ["dx-app"]
toc = true
+++

Inspect Image and Packaging (iip) inspects packages (python, ruby, node, etc) and images (docker) of a project.

### Install

To install iip you will need to build it from source (more info on the [iip gitlab page](https://gitlab.com/drad/iip).

- clone: `git@gitlab.com:drad/iip.git`
- build: `cd iip && cargo build --release && strip target/release/iip && strip target/release/iip`
- create symlink (optional): `ln -sf -T "$HOME/path-to-your-clone/iip/target/release/iip" "$HOME/bin/iip"`
    + this step is optional and simply creates a symlink in your `~/bin` directory for iip which basically gets iip on your `$PATH`

### Setup

iip uses a `.iip` (toml) config file. This file needs to be in the directory of the service you want to run iip for. For example, say you have a project named [points](https://gitlab.com/dx-koders/points) and this project has the following components:

- api: a fastapi service which accepts REST requests and performs actions against a couchdb database
- db: the couchdb database
- redis: the redis instance
- ui: a quasar (vuejs) front-end for the application

The `api` service has its own Dockerfile and python requirements. To run iip for this service we need a `.iip` (config file) for this service. You can generate a `.iip` config file by using the `iip --init` option. This will create a skeleton config file which can then be updated to perform the desired actions.

You may also want to run `iip --init` in the `ui` directory as well to use iip to inspect the ui service as well.

### Running

Run iip by issuing the `iip` command in the directory of the service you wish to inspect (same directory as the `.iip` config file). This will perform the actions enabled in the `.iip` config file for the service in question.

Example run:

![Run](run.png)
