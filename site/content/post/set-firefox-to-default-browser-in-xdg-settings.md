+++
author = "drad"
title = "Set Firefox to Default Browser in xdg Settings"
date = "2020-11-12 06:46:51"
description = "how to set firefox nightly as default browser with xdg settings"
featured = false
tags = ["firefox", "chromium", "chrome", "xdg", "linux"]
categories = ["howto"]
+++

If you use firefox nightly and chromium you've likely ran into the issue where some apps launch chromium as the default browser for URLs. This is ultimately due to firefox nightly not having a true installer which would create the firefox.desktop file that xdg-settings uses to set/determine the browser to open on xdg-open events.

To verify the current settings you can use: `xdg-settings get default-web-browser` which will likely return `chromium.desktop`.

To change this, you will need to create a `firefox.desktop` file and use the xdg-settings command:

- in a terminal, clear BROWSER: `export BROWSER=""`
    + just to make sure it is not set which can cause other issues
- create a firefox.desktop file in `/usr/share/applications/` with the following content:

```
[Desktop Entry]
Version=1.0
Name=Firefox Browser
GenericName=Web Browser
Comment=Access the Internet
Exec=/PATH_TO_FIREFOX_BIN/firefox %U
StartupNotify=true
Terminal=false
Icon=firefox-browser
Type=Application
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml_xml;image/webp;x-scheme-handler/http;x-scheme-handler/https;x-sch>
Actions=new-window;new-private-window;

[Desktop Action new-window]
Name=New Window
Exec=/PATH_TO_FIREFOX_BIN/firefox

[Desktop Action new-private-window]
Name=New Incognito Window
Exec=/PATH_TO_FIREFOX_BIN/firefox --incognito
```
**NOTE:** you will need to change /PATH_TO_FIREFOX_BIN/ to the path to your firefox binary.

- change xdg to use firefox: `xdg-settings set default-web-browser firefox.desktop`
- check xdg setting: `xdg-settings get default-web-browser`
- test the change: `xdg-open "http://dradux.com"`
    + this should open the URL in firefox

Also note you should check the default application (update-alternatives) gnome and default browsers settings:

- default: `update-alternatives --config x-www-browser`
- gnome: `update-alternatives --config gnome-www-browser`
- kde: check System Settings > Applications > Default Applications > Web Browser

### Links ###

- [stackexchange: cant change xdg open url handler](https://unix.stackexchange.com/questions/307641/cant-change-the-xdg-open-url-handler-to-firefox)
- [stackoverflow:xdg settings not setting default browser](https://stackoverflow.com/questions/52209774/xdg-settings-not-setting-default-web-browser-in-gentoo)
- [debian wiki: default web browser](https://wiki.debian.org/DefaultWebBrowser)
