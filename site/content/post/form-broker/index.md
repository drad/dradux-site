+++
author = "drad"
title = "form-broker - easily handle form submissions"
date = "2022-05-27"
description = "an API that handles form submissions, sending the submission to configurable endpoints (email, database, rocketchat, etc.)"
featured = false
tags = ["k8s", "docker", "python", "fastapi", "form submission", "email", "database", "rocketchat"]
usePageBundles = true
featureImage = 'example.png'
# featureImageAlt: 'Description of image' # Alternative text for featured image.
# featureImageCap: 'This is the featured image.' # Caption (optional).
# thumbnail: 'thumbnail.jpg' # Image in lists of posts.
# shareImage: 'share.jpg' # For SEO and social media snippets.
categories = ["dx-app"]
+++

form broker (fbkr) is an API that handles form submissions, sending the submission to configurable endpoints (email, database, rocketchat, etc.). fbkr was designed to handle any form submission and works well when combined with Static Site Generators such as [Nikola](https://getnikola.com/) or [Hugo](http://gohugo.io/).

This site uses a fbkr instance, to try it out [send us a message](/contact)!

### More Info

Check out [form-broker](https://gitlab.com/dx-koders/form-broker) for more info.
