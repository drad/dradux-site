+++
author = "drad"
title = "boi Tutorial"
date = "2019-08-03 22:02:50"
description = "a tutorial on using boi to build and deploy an application to kubernetes"
featured = false
tags = ["boi", "ibuilder", "tutorial", "devops", "ci", "cd", "builds", "docker", "python"]
categories = ["tutorial"]
toc = true
+++

{{% notice note "2022-05-27 Update" %}}
This article references boi which was rebranded to be [ibuilder](https://gitlab.com/dradux/ibuilder) in May of 2022.
{{% /notice %}}

This tutorial will show how to take an existing application (the dradux.com website itself) and deploy it to a kubernetes cluster using [boi](https://gitlab.com/drad/boi), a lightweight tool for building and pushing docker images. Deploying an application to k8s can be a daunting task depending on the complexity of your application and the needs therein; however, as this post shows, it can be quick (~30 minutes) and relatively easy if you have the needed components in place.

## Key Components

- an application (ready to go)
- a k8s cluster
- an image repository


### The Application

For this tutorial I am using the dradux.com website itself as an example. Its [source is on gitlab](https://gitlab.com/drad/dradux-site). The app is a nikola based static site. To 'generate' the site you would simply run `nikola build`, the built site is located in the `output/` directory. This part was easy as it already existed!


### The k8s Cluster

Easy, I already had one. If you don't, I suggest looking at [Rancher](https://rancher.com) as you can get a k8s cluster set up in minutes.


### The Image Repository

I have spent countless hours setting up internal registries, registries inside of k8s, and integrating with external/public registries. If your app is close-sourced this task will take more time and be more difficult. As of late I have started using the registry which comes with a gitlab project as it is free and I do not need to manage the registry. If you want to see one, check our the [dradux-site](https://gitlab.com/drad/dradux-site/container_registry) registry.


## Dockerizing Your App (if needed)

This is an art in itself and can take on a life of its own. I suggest a little planning before doing this and keep things simple. Your application/code should do the heavy work and docker should be a light wrapper to bundle it all up but some things (old java apps) just wont stay light in my experience. If you need help in dockerizing feel free to contact us!

Dockerizing dradux-site was simple as the site is a static site - just html/javascript/css. No server-side scripting, database, etc. All we need is an --nginx-- lighttpd instance, add the app code and we are ready to go!

Feel free to take a look at the [Dockerfile](https://gitlab.com/drad/dradux-site/-/blob/master/app/Dockerfile) along with its [entrypoint.sh](https://gitlab.com/drad/dradux-site/-/blob/master/app/entrypoint.sh) (used to run rsyslogd which sends my web logs into a graylog server) and the [lighttpd.conf](https://gitlab.com/drad/dradux-site/-/blob/master/app/.config/lighttpd/lighttpd.conf).

That's all it took to dockerize the app!

## Add boi Integration

As mentioned before [boi](https://gitlab.com/drad/boi) is lightweight. To add boi to the project I created the [.boi.toml](https://gitlab.com/drad/dradux-site/-/blob/master/app/.boi.toml) configuration file. That is pretty much it, now you can build your image and push to your image registry.


## Build & Push Your First Image

Use boi to build and push an image of the application: `boi build --version=1.0.0`. If all goes well you will have version 1.0.0 of your application in the container registry specified.


## Deploy to k8s

Before you can deploy to k8s you need to setup your deployment. This can be done in several different ways in k8s, we will use a standard namespaced deployment as an example.

First, create your namespace and then create the deployment:
```yaml
apiVersion: v1
kind: List
items:
- apiVersion: apps/v1beta1
  kind: Deployment
  metadata:
    name: dradux-site
    namespace: dradux-site
  spec:
    template:
      metadata:
        labels:
          run: dradux-site
      spec:
        containers:
          - name: dradux-site
            image: registry.gitlab.com/drad/dradux-site:latest
            resources:
              limits:
                memory: 128Mi
                cpu: 100m
            ports:
              - containerPort: 80
- apiVersion: v1
  kind: Service
  metadata:
    name: dradux-site
    namespace: dradux-site
    labels:
      run: dradux-site
  spec:
    ports:
    - port: 80
    selector:
      run: dradux-site
```
Notice that the image for the container is set to where we push to with boi - also note we are pulling the 'latest' tag and in boi we have it set to apply the 'latest' tag on build.

Next, create an ingress to route traffic into the service:
```yaml
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: default
  namespace: dradux-site
spec:
  rules:
  - host: dradux.com
    http:
      paths:
      - path: /
        backend:
          serviceName: dradux-site
          servicePort: 80

```
A standard ingress, we will SSL terminate at a LoadBalancer in front of k8s.

## Summary

You should now have the service up and an ingress to catch the traffic inside of k8s. You still have the task of managing the DNS to route dradux.com to the k8s cluster (and SSL termination at a LB if needed) but other than that you should be able to hit your URL and see your site!
