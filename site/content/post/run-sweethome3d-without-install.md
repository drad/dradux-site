+++
author = "drad"
title = "Run SweetHome3D Without Install"
date = "2019-08-05 16:38:42"
description = "how to run SweetHome3D without install"
featured = false
tags = ["apps", "sweethome3d", "java"]
categories = ["howto"]
+++

### About ###
SweetHome3D is a free interior design application. You can install it from most repos; however, the install often requires a lot of unneeded libraries and/or didn't work well for my situation. There are several articles that suggest you can/should run it directly but none gave the steps needed so I thought I would pull together a quick post on it.

It's not difficult or glorious, hopefully the info will be of use to others. Here are the steps:

1. download [SweetHome3D](http://www.sweethome3d.com/download.jsp)
1. extract files: `tar -xzf SweetHome3D-*.tgz`
1. run the jar: `./SweetHome`
  - note: you will need a JRE - openjdk works just fine

### Lesson's Learned ###
I did manage to produce a working docker sweethome3d which keeps you from installing anything (other than docker); however, the java GUI blacks out sections of the app from time to time and wasn't worth the effort given I already have java installed for [dbeaver](https://dbeaver.io/). If you are interested in getting it to run in docker feel free to start with what I produced: [https://gitlab.com/drad/docker-sweethome3d](https://gitlab.com/drad/docker-sweethome3d).
