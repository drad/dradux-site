+++
author = "drad"
title = "Setup SAR on Alpine Linux"
date = "2020-10-16 11:48:22"
description = ""
featured = false
tags = ["sar", "sysstat", "alpine linux", "cron", "sysadmin", "monitoring"]
categories = ["sysadmin", "howto"]
+++

There are several good articles on setting up `SAR` for debian ([sar on ubuntu/debian](https://www.crybit.com/sysstat-sar-on-ubuntu-debian/)) and redhat systems but I could find few on Alpine (perhaps because it is simple).

In any event, here's my process for setting up sar to run periodically via cron to track server stats.

### Install ###

Install with: `apk add sysstat`

### Setup ###

Not a lot of setup is required, you may want to review the `/etc/sysconfig/sysstat` file for options.

### Running Via cron ###

Create a cron task as you like to kick off sar periodically, the following will run sar every 2 minutes.

```bash
# Run sar to gather stats
*/2 * * * *   /usr/lib/sa/sa1 1 1
# Additional run at 23:59 to rotate the statistics file
59 23 * * *   /usr/lib/sa/sa1 60 2
```

### Logs ###

Alpine's sysstats logs are in `/var/log/sa`
