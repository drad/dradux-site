+++
author = "drad"
title = "Media Player Broker"
date = "2022-05-24"
description = "Media Player Broker (mpb) is an application that helps you play and track media you have watched over disparet locations"
featured = true
tags = ["lightweight", "minimalist", "media", "video", "cli", "python"]
usePageBundles = true
featureImage = 'help.png'
# featureImageAlt: 'Description of image' # Alternative text for featured image.
# featureImageCap: 'This is the featured image.' # Caption (optional).
# thumbnail: 'thumbnail.jpg' # Image in lists of posts.
# shareImage: 'share.jpg' # For SEO and social media snippets.
categories = ["dx-app"]
toc = true
+++

Media Player Broker (mpb) is an application that helps you play and track media you have watched over disparet locations. mpb keeps track of what you have played at Location A so when you are at Location B you can see what you have watched from either location to avoid digging through history command output over SSH.

mpb is not a player itself but it can be configured to launch your player of choice to view media.

### Install

Install via pip or pipx: `pipx install mpbroker` or view the [mpbroker gitlab page](https://gitlab.com/drad/mpbroker).

### Library Info

The `info` command shows Library info:

![Library Info](info.png)

### Listing Media

Listing media supports pagination and can be filtered by `--new`, `--played`, and `--watched`.

![Library List](list.png)


You can install mpb via pip or pipx: `pipx install mpbroker` or view the [mpbroker gitlab page](https://gitlab.com/drad/mpbroker).
