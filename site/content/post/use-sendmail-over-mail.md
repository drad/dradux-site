+++
author = "drad"
title = "Use sendmail over mail"
date = "2019-12-23 21:13:41"
description = "how and why to use sendmail rather than mail for sending system mail"
featured = false
tags = ["mail", "sendmail", "sysadmin",]
categories = ["howto"]
toc = true
+++

mail (from mailutils) is a common sysadmin tool for sending system mail, however, it brings in unneeded/undesired packages (e.g. mysql/mariadb tools). sendmail is much lighter and can usually get the job done.

### Typical mail Scenario ###

The following cron entry runs deborphan every Monday at 14:33, emailing the results to the drad user.

```bash
33 14 * * Mon   deborphan --guess-all | mail -s "Weekly deborphan Report" drad
```

### Using sendmail Instead ###

sendmail does not have a -s (subject parameter). You need to preface your message with "Subject: {your subject}" followed by a new line and then the body such as follows:

```bash
33 14 * * Mon   ( echo "Subject: Weekly deborphan Report"; echo; deborphan --guess-all ) | /usr/sbin/sendmail drad
```

The sendmail route is a little more verbose but gets the job done and now you do not need mailutils!


### Summary ###
mail (mailutils) is a common and useful mail tool but depends on several packages which may not be desirable. You can fully replace mail with sendmail to avoid the dependencies and yet provide the same system mail functionality.
