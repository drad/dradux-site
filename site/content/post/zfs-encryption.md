+++
author = "drad"
title = "ZFS Encryption"
date = "2020-12-03 20:21:38"
description = "Basic guide on Encrypted ZFS Datasets"
featured = false
tags = ["zfs", "encryption", "security"]
categories = ["security"]
+++


- enable ZFS Encryption feature: `zpool set feature@encryption=enabled tank`

### Create Encrypted Dataset with Passphrase

- create dataset: `zfs create -o encryption=on -o keyformat=passphrase tank/tmp`
    + this will prompt you for the passphrase
    + after rebooting, you will need to load the passphrase to mount the volume such as: `zfs load-key tank/tmp` after which you can use `zfs mount tank/tmp` to mount it

- create encrypted dataset with key file:
    + create a key: `dd if=/dev/random of=/root/.keys/mykey`
    + create dataset: `zfs create -o encryption=on -o keyformat=raw -o keylocation=file:///root/.keys/mykey tank/my-new-dataset`
