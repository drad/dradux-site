+++
author = "drad"
title = "Minimalistic Coding Standard"
date = "2020-10-27 06:43:54"
description = "the Minimalistic Coding Standard"
featured = false
tags = ["minimalistic", "minimalist", "coding", "standard", "development", "design"]
+++

Minimalistic applications require sound design and development which produce applications that are easier to run, use, support, and maintain over the life of the application.

The Minimalistic Coding Standard (MCS) is a simple coding (design, development, deployment) philosophy which emphasizes minimalism in all aspects of coding.

Core concepts of MCS:

- simple over complex
- one good way over multiple ways
- simply secure over not

KIM = Keep it minimal
