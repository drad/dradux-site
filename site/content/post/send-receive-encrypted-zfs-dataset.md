---
title: "Send Receive Encrypted ZFS Dataset and All Snapshots" # Title of the blog post.
date: 2022-06-06T06:17:10-04:00 # Date of post creation.
description: "Article description." # Description used for search engine.
featured: false # Sets if post is a featured post, making appear on the home page side bar.
draft: false # Sets whether to render this page. Draft of true will not be rendered.
toc: false # Controls if a table of contents should be generated for first-level links automatically.
# menu: main
usePageBundles: false # Set to true to group assets like images in the same folder as this post.
#featureImage: "/images/path/file.jpg" # Sets featured image on blog post.
#featureImageAlt: 'Description of image' # Alternative text for featured image.
#featureImageCap: 'This is the featured image.' # Caption (optional).
#thumbnail: "/images/path/thumbnail.png" # Sets thumbnail image appearing inside card on homepage.
#shareImage: "/images/path/share.png" # Designate a separate image for social media sharing.
codeMaxLines: 10 # Override global value for how many lines within a code block before auto-collapsing.
codeLineNumbers: false # Override global value for showing of line numbers within code block.
figurePositionShow: true # Override global value for showing the figure label.
categories:
  - security
tags:
  - zfs
  - encryption
  - security
# comment: false # Disable comment if false.
---

Moving an Encrypted Dataset is not a complicated task but can require a little work if you have snapshots which you want to move as well. A handy tip to accomplish this is to create a recursive snapshot before moving and then send this snapshot to get all moved easily.

{{% notice tip "Tip" %}}
Create a recursive snapshot and send this snapshot to send all snapshots:
`zfs snap -r tank/archive@move-dataset`
Now send this recursive snapshot:
`zfs send -Rw tank/archive@move-dataset | zfs recv -Fuv tank2/archive`
{{% /notice %}}

### Steps to Move Encrypted Dataset with All Snapshots

1. create a recursive snapshot (so we can transfer all snapshots): `zfs snap -r tank/archive@move-dataset`
2. send snapshot: `zfs send -Rw tank/archive@move-dataset | zfs recv -Fuv tank2/archive`
3. verify/check received snapshots: `zfs list -t snapshot tank2/archive`
4. verify/check keys (if keylocation is blank set it accordningly): `zfs get encryption,keylocation,keyformat tank2`
    - if keylocation is blank set accordingly: `zfs set keylocation=file:///your/key/location tank2/archive`
5. after send and checking mountpoint the mount (`zfs mount tank/archive`) will likely fail with 'encryption key not loaded', to fix this load the key: `zfs load-key tank2/archive`
6. mount new location: `zfs mount tank2/archive`
7. check snapshots and data

I run a non-systemd system so I also need to add the loadkey to my ZFS init script (`/etc/init.d/zfs`) to ensure the key is loaded at system start.

{{% notice note "Note" %}}
Its a good idea to reboot and make sure the dataset is automounted and there are no issues.
{{% /notice %}}
