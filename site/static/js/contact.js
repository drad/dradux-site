jQuery(document).ready(function (e) {
/* Contact form */
$(function () {
  $('#contact-form').find('input,select,textarea,button').jqBootstrapValidation({
    preventSubmit: true,
    submitError: function ($form, event, errors) {
    },
    submitSuccess: function ($form, e) {
      resetAfterSuccessfulSubmit = true;
      e.preventDefault()
      var submitButton = $('button[type=submit]', $form);
      var contact_service_url = 'http://localhost:8029/api/v2/posts/form/39ff3d8e-352b-4af3-b332-020481b6db77';
      $.ajax({
        type: 'POST',
        crossDomain: true,
        // Set real action URL
        url: contact_service_url,
        data: $form.serialize(),
        beforeSend: function (xhr, opts) {
          if ($('#_email', $form).val()) {
            xhr.abort()
          } else {
            submitButton[0].innerHTML = "Sending...";
            submitButton[0].disabled = true;
          }
        },
        error: function (err) {
            console.log("Got an Error:", err);
            submitButton[0].innerHTML = "ERROR SENDING, please try again";
            submitButton[0].disabled = false;
        }
      }).done(function (data) {
        console.log("- response:", data);
        if (data.success == true ) {
            submitButton[0].innerHTML = "Sent...Thank You!";
            submitButton[0].disabled = false;
            // reset form values after success.
            if (resetAfterSuccessfulSubmit) {
                $('input[name=name]', $form)[0].value = '';
                $('input[name=email]', $form)[0].value = '';
                $('input[name=subject]', $form)[0].value = '';
                $('textarea[name=message]', $form)[0].value = '';
            }
        } else {
            submitButton[0].innerHTML = "ERROR SENDING, please try again";
            submitButton[0].disabled = false;
        }
      })
    },
    filter: function () {
      return $(this).is(':visible')
    }
  })
})
})

$('#name').focus(function () {
$('#success').html('')
})
