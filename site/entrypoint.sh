#!/bin/sh

echo "- starting rsyslogd daemon..."
rsyslogd

echo "- set contact form access_key"
echo "  --> contact_service_url=${APP_CONTACT_URL}"
original_str="^      var contact_service_url = '.*';$"
replace_str="      var contact_service_url = '${APP_CONTACT_URL}';"
sed -i "s|${original_str}|${replace_str}|g" "/var/www/localhost/htdocs/js/contact.js"

echo "- fix logging..."
# all access to log stream
chmod a+w /dev/pts/0

echo "starting lighttpd..."
exec lighttpd -D -f /etc/lighttpd/lighttpd.conf

#echo "just keep container running..."
#tail -f /dev/null
