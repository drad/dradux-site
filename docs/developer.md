# Developer

### Development

- run with internal server: `hugo serve -D`
    + run this from the `website/site` directory (hugo root)
    + internal server is hot-reload capable

### Building

- normal build: `hugo --cleanDestinationDir --enableGitInfo --minify --environment "Production" --gc`
    - note: this will create the `public` directory and its contents which are needed in the image

### Generate webp Images

- a quality of 75 should work for most images: `cwebp -q 75 source.jpg -o destination.webp`
    + you may want/need higher quality (85-95) for special cases
    + [more info](https://www.tecmint.com/convert-images-to-webp-format-in-linux/) on webp

### How To For Developer Items

#### Create Blog Hero Image
