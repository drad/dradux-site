from locust import HttpUser, TaskSet, task
# install: `pip install locustio`
# start server: `locust -f locustfile_base.py`
# run/monitor test: http://localhost:8089/

class WebsiteTasksBasic(TaskSet):

    @task(5)
    def index(self):
        self.client.get("/")

    @task(2)
    def draduxos(self):
        self.client.get("/draduxOS")

    @task(3)
    def blog(self):
        self.client.get("/post/index.html")

    @task(3)
    def blog_sendmail(self):
        self.client.get("/post/use-sendmail-over-mail/")

    @task(2)
    def blog_sysmon(self):
        self.client.get("/post/system-resource-monitoring/")

    # ~ @task(1)
    # ~ def archives(self):
        # ~ self.client.get("/archive.html")

    @task(3)
    def tags(self):
        self.client.get("/categories/index.html")

    @task(1)
    def rss(self):
        self.client.get("/index.xml")

    @task(2)
    def about(self):
        self.client.get("/about")

    @task(1)
    def sitemap(self):
        self.client.get("/sitemap.xml")



class WebsiteUser(HttpUser):
    tasks = {WebsiteTasksBasic}
    # ~ host = "http://localhost:8000"
    host = "https://dradux.com"

    min_wait = 5 * 1000   #5 seconds
    max_wait = 15 * 1000   #15 seconds
