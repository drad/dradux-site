# README

This is the dradux.com website.

### Notices

- none

### Stack

- hugo
- lighttpd
- designed to deploy in k8s via helm

### Deployment

#### k8s/helm

This application is fully deployable via helm, go to the `.devops/helm` directory and perform the following to deploy:

- configure the `values.yaml.prod` file per your needs
- deploy the chart: `helm install dx-site dx-site/ --values dx-site/values.yaml.prod --create-namespace --namespace dx-site --disable-openapi-validation`
  - update: `helm upgrade dx-site dx-site/ --values dx-site/values.yaml.prod --namespace dx-site --disable-openapi-validation`
  - uninstall: `helm uninstall -n dx-site dx-site`
  - NOTICE: after deploying the updated chart you need to rollout the change to the service impacted:
    + `kubectl rollout restart -n dx-site deployment dx-site`


### Links

- [hugo](https://gohugo.io)
- [Clarity](https://themes.gohugo.io/themes/hugo-clarity/): a simple, clean, very usable theme
